这是一个根据快盘API实现的上传的脚本，分别使用PHP和shell实现的，可用于cron job等场合。

目录结构：

    php/  #php脚本实现
        -include/   #一些类库
        -cli.php    #php的cli模式运行它，上传部分的最终实现
        -common.inc.php  #常量和配置
       -auth.php  #获取授权的脚本


    shell/   #shell实现
        -backup.sh            #批量执行入口，根据个人的需要进行修改
        -kuaipan_uploader.sh  #快盘的上传实现部分

    tmp/  #临时存放上传打包文件
    
