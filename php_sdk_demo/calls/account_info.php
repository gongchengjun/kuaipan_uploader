<?php
/**
 * 查看用户信息
 * 
 * $kp = new Kuaipan('consumer_key', 'consumer_secret');
 */
$ret = $kp->api('account_info');
if (false === $ret) {
    $ret = $kp->getError();
}
return $ret;