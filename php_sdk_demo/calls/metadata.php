<?php
/**
 * 获取用户快盘的根目录的信息
 * 
 * 
 * $kp = new Kuaipan('consumer_key', 'consumer_secret');
 */
$root_path = 'kuaipan'; //应用拥有整个快盘的权限，否则可以使用ap_folder
$ret = $kp->api ( 'metadata', $root_path, $params );
if (false === $ret) {
    $ret = $kp->getError ();
}
return $ret;