<?php
/**
 * 创建一个文件夹 (new_folder_<timestamp>)
 * 
 * 
 * $kp = new Kuaipan('consumer_key', 'consumer_secret');
 */
$params = array (
        'root' => 'kuaipan',
        'path' => 'new_folder_' . time () 
);
$ret = $kp->api ( 'fileops/create_folder', '', $params );
if (false === $ret) {
    $ret = $kp->getError ();
}
return $ret;