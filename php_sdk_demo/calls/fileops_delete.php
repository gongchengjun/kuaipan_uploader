<?php
/**
 * 删除一个文件
 * 
 * 
 * $kp = new Kuaipan('consumer_key', 'consumer_secret');
 */
$params = array (
        'root' => 'kuaipan',
        'path' => 'folder_to_delete'
);
$ret = $kp->api ( 'fileops/delete', '', $params );
if (false === $ret) {
    $ret = $kp->getError ();
}
return $ret;